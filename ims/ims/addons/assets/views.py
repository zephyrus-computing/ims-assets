from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.auth.decorators import login_required, permission_required
from django.conf import settings
from django.core.exceptions import PermissionDenied
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.shortcuts import render, get_object_or_404, redirect
import logging

from inventory.search import Search
import ims.addons.assets.helpers as helpers
from ims.addons.assets.models import Asset, AssetDetail, CustomField
from log.logger import getLogger

logger = logging.getLogger(__name__)
logger2 = getLogger('tracker')

def asset_list(request):
    cname = 'asset_list'
    logger.debug('Request made to {} view'.format(cname))
    logmsg = 'Accessed Asset List'
    logger2.debug((request.user or 0), 'Request to view {}'.format(cname))
    if settings.ALLOW_ANONYMOUS or request.user.is_authenticated:
        # Get all PartThreshold
        logger.debug('Generating object_list of Assets')
        object_list = Asset.objects.all()

        # Create the Search filter and apply to the existing results
        query = request.GET.get('q')
        if query != None: 
            logger.debug('Filtering object_list of Assets with query')
            logmsg = '{} with query \'{}\''.format(logmsg, query)
            querytype = request.GET.get('t')
            qt = helpers.parse_querytype(querytype)
            sf = Search().filter(['name', 'detail__value', 'description'], qt + query)
            object_list = object_list.filter(sf)

        # Create the Paginator and get the existing page
        logger.debug('Paginate object_list of Asset')
        paginator = Paginator(object_list, settings.ADDON_ASSETS_PAGE_SIZE)
        page = request.GET.get('page')
        try:
            assets = paginator.page(page)
        except PageNotAnInteger:
            logger.debug('PageNotAnInteger, show page 1')
            assets = paginator.page(1)
        except EmptyPage:
            logger.debug('EmptyPage, show page {}'.format(paginator.num_pages))
            assets = paginator.page(paginator.num_pages)

        # Return the list of partthresholds
        logger.debug('Rendering request for {} view'.format(cname))
        logger2.info((request.user or 0), logmsg)
        return render(request, 'assets/list.html', {'query': query, 'page': page, 'assets': assets})
    else:
        logger.debug('User was not authenticated and ALLOW_ANONYMOUS is set to False')
        logger2.warning(0, 'Unauthenticated request to {}'.format(request.path))
        return redirect('/accounts/login/?next=' + request.path)
    
def asset_detail(request, id):
    cname = 'asset_detail'
    logger.debug('Request made to {} view'.format(cname))
    logger2.debug((request.user or 0), 'Request to view Asset Details for id {}'.format(id))
    if settings.ALLOW_ANONYMOUS or request.user.is_authenticated:
        # Get the Asset object to display in detail
        asset = get_object_or_404(Asset, id=id)
        logger.debug('Rendering request for {} view'.format(cname))
        logger2.info((request.user or 0), 'Accessed Asset id {}'.format(id))
        return render(request, 'assets/detail.html', {'asset': asset})
    else:
        logger.debug('User was not authenticated and ALLOW_ANONYMOUS is set to False')
        logger2.warning(0, 'Unauthenticated request to {}'.format(request.path))
        return redirect('/accounts/login/?next=' + request.path)

def ajax_asset_move(request, id, sid):
    pass
