import django
django.setup()
from django.contrib.auth.models import AnonymousUser, User
from django.test import TestCase, override_settings
from random import randint
from rest_framework import status
from inventory.models import Part, Storage, PartStorage
from ims.addons.assets.models import Asset, AssetDetail, CustomField
import ims.addons.assets.helpers as helpers

baseurl = '/assets/'

# Test assets views
class AssetsAssetViewTests(TestCase):
    def setUp(self):
        for i in range(0,101):
            part = Part.objects.create(name='Test_View_Part_{}'.format(i), description='This is a view TestCase test part', sku=str(i).zfill(5), price=10.00, cost=5.00)
            storage = Storage.objects.create(name='Test_View_Storage_{}'.format(i), description='This is a view TestCase test storage')
            PartStorage.objects.create(part=part,storage=storage,count=5)
            Asset.objects.create(part=part,storage=storage,assetid=str(i).zfill(4),serialnumber=str(i).zfill(4))
        for i in helpers.WIDGETS:
            CustomField.objects.create(name='Test_View_CustomField_{}'.format(i), description='This is a view TestCase test customfield',widget=i)
        for i in range(0,20):
            j = randint(0,100)
            k = randint(10,20)
            asset = Asset.objects.all()[i]
            for l in range(0,k):
                m = randint(0,len(helpers.WIDGETS)-1)
                customfield = CustomField.objects.all()[m]
                AssetDetail.objects.create()

        self.baseurl = '{}asset/'.format(baseurl)

    def test_view_assets(self):
        # Test the default view
        response = self.client.get(self.baseurl)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'assets/asset/list.html')
        # Test the pagination of the view
        response = self.client.get('{}?page=-1'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.request.get('QUERY_STRING'), 'page=-1')
        response = self.client.get('{}?page=100'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.request.get('QUERY_STRING'), 'page=100')
        response = self.client.get('{}?q=test'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.request.get('QUERY_STRING'), 'q=test')
        # Test Anonymous Access Restriction
        with override_settings(ALLOW_ANONYMOUS = False):
            response = self.client.get(self.baseurl)
            self.assertRedirects(response, '/accounts/login/?next={}'.format(self.baseurl))
            self.client.force_login(User.objects.get_or_create(username='testuser')[0])
            response = self.client.get(self.baseurl)
            self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_view_part_details(self):
        # Test a valid part
        i = randint(0,100)
        p = Asset.objects.all()[i]
        response = self.client.get('{}{}/'.format(self.baseurl, p.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'assets/asset/detail.html')
        # Test an invalid part
        response = self.client.get('{}idonotexist/'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        # Test Anonymous Access Restriction
        with override_settings(ALLOW_ANONYMOUS = False):
            response = self.client.get('{}{}/'.format(self.baseurl, p.id))
            self.assertRedirects(response, '/accounts/login/?next={}{}/'.format(self.baseurl, p.id))
            self.client.force_login(User.objects.get_or_create(username='testuser')[0])
            response = self.client.get('{}{}/'.format(self.baseurl, p.id))
            self.assertEqual(response.status_code, status.HTTP_200_OK)
