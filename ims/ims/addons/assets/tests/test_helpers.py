import django
django.setup()
from datetime import datetime, timedelta
from django.conf import settings
from django.contrib.auth.models import User
from django.db.models import Q
from django.test import TestCase, override_settings
from random import randint, choice
import django.forms.widgets as widgets
from inventory.models import Part, PartStorage, Storage
from ims.addons.assets.models import Asset, AssetDetail, CustomField
import ims.addons.assets.helpers as helpers
from log.models import Log

class LookupMethodsUnitTests(TestCase):
    def setUp(self):
        pass

    def test_get_widget(self):
        for i in helpers.WIDGETS:
            self.assertEqual(i[1], helpers.get_widget(i[0]))
        for i in ('Cog','Gear','Thingy'):
            self.assertEqual('unknown', helpers.get_widget(i))

    def test_get_widget_type(self):
        for i in helpers.WIDGETS:
            self.assertEqual(i[0], helpers.get_widget_type(i[1]))
        for i in ('cg','ge','ar','ty'):
            self.assertEqual('', helpers.get_widget_type(i))

    def test_get_widget_class(self):
        for i in helpers.WIDGETS:
            name = i[1]
            widget = helpers.get_widget_class(i)
            self.assertEqual(widget.__module__, 'django.forms.widgets')
            self.assertIn(name.split(' ')[0], type(widget).__name__)
        #Number Input - ni
        widget = helpers.get_widget_class('ni')
        self.assertEqual(widgets.NumberInput, type(widget))
        #Password Input - pi
        widget = helpers.get_widget_class('Password Input')
        self.assertEqual(widgets.PasswordInput, type(widget))
        #Null Boolean Select - ns
        widget = helpers.get_widget_class('ns')
        self.assertEqual(widgets.NullBooleanSelect, type(widget))
        #File Input - fi
        widget = helpers.get_widget_class('File Input')
        self.assertEqual(widgets.FileInput, type(widget))
        #Bad Input - bi
        widget = helpers.get_widget_class('My Bad')
        self.assertEqual(widgets.TextInput, type(widget))
    
    def test_parse_querytype(self):
        result = helpers.parse_querytype(1)
        self.assertEqual(result, '^')
        result = helpers.parse_querytype('1')
        self.assertEqual(result, '^')
        result = helpers.parse_querytype(2)
        self.assertEqual(result, '=')
        result = helpers.parse_querytype('2')
        self.assertEqual(result, '=')
        result = helpers.parse_querytype(3)
        self.assertEqual(result, '')
        result = helpers.parse_querytype('3')
        self.assertEqual(result, '')
        result = helpers.parse_querytype('banana')
        self.assertEqual(result, '')
        result = helpers.parse_querytype([])
        self.assertEqual(result, '')
        result = helpers.parse_querytype(None)
        self.assertEqual(result, '')

    def test_is_int(self):
        for i in range(0,10):
            j = randint(0,9999)
            self.assertTrue(helpers.is_int(j))
        for i in 'these','are','not','ints':
            self.assertFalse(helpers.is_int(i))
        for i in 1.0,3.14,'-1':
            self.assertTrue(helpers.is_int(i))
