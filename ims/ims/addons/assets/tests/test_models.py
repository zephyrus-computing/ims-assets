import django
django.setup()
from django.contrib.auth.models import User
from django.test import TestCase, override_settings
from django.urls import reverse
from random import randint
from inventory.models import Part, PartStorage, Storage
from ims.addons.assets.models import Asset, AssetDetail, CustomField
import ims.addons.assets.helpers as helpers

class AssetUnitTests(TestCase):
    def setUp(self):
        for i in range(1,11):
            part = Part.objects.create(name='test_part_{}'.format(i),description='This is a TestCase test part',sku=str(i).zfill(4),price=9.99,cost=4.99)
            storage = Storage.objects.create(name='test_storage_{}'.format(i),description='This is a TestCase test storage')
            ps = PartStorage.objects.create(part=part,storage=storage,count=5)
            assetid = str(randint(0,9999)).zfill(4)
            Asset.objects.create(assetid=assetid,serialnumber=assetid,part=part,storage=storage)
        
    def test_asset_str(self):
        asset = Asset.objects.first()
        self.assertEqual(str(asset),'{}-{}'.format(asset.assetid,asset.serialnumber))
        asset = Asset.objects.last()
        self.assertNotEqual(str(asset),'Test-Asset')

    def test_asset_absolute_url(self):
        asset = Asset.objects.first()
        self.assertEqual(asset.get_absolute_url(), reverse('ims.addons.assets:asset_detail',args=[asset.id]))
        asset = Asset.objects.last()
        self.assertNotEqual(asset.get_absolute_url(), reverse('ims.addons.assets:asset_detail',args=[0]))

    def test_asset_move(self):
        asset = Asset.objects.first()
        storage = Storage.objects.last()
        if storage == asset.storage:
            storage = Storage.objects.first()
        old = asset.storage
        asset.move(storage)
        asset.refresh_from_db()
        self.assertNotEqual(asset.storage, old)
        self.assertEqual(asset.storage, storage)
        asset.move(old.id)
        asset.refresh_from_db()
        self.assertEqual(asset.storage, old)
        with self.assertRaises(Storage.DoesNotExist):
            asset.move(0)

class CustomFieldUnitTests(TestCase):
    def setUp(self):
        for i in range(11,21):
            j = randint(0,len(helpers.WIDGETS))
            CustomField.objects.create(name='Test_CustomField_{}'.format(i),description='This is a TestCase test customfield',required=True,regex='\s+',widget=helpers.WIDGETS[j])

    def test_customfield_str(self):
        i = randint(11,20)
        customfield = CustomField.objects.get(name='Test_CustomField_{}'.format(i))
        self.assertEqual(str(customfield),'Test_CustomField_{}'.format(i))

class AssetDetailUnitTests(TestCase):
    def setUp(self):
        for i in range(0,len(helpers.WIDGETS)):
            CustomField.objects.create(name='Test_CustomField_{}'.format(str(i).zfill(4)),description='This is a TestCase test customfield',required=True,regex='\s+',widget=helpers.WIDGETS[i])
        for i in range(21,31):
            part = Part.objects.create(name='test_part_{}'.format(i),description='This is a TestCase test part',sku=str(i).zfill(4),price=9.99,cost=4.99)
            storage = Storage.objects.create(name='test_storage_{}'.format(i),description='This is a TestCase test storage')
            ps = PartStorage.objects.create(part=part,storage=storage,count=5)
            assetid = str(randint(0,9999)).zfill(4)
            asset = Asset.objects.create(assetid=assetid,serialnumber=assetid,part=part,storage=storage)
            for j in range(0,randint(3,len(helpers.WIDGETS)-1)):
                k = randint(0,len(helpers.WIDGETS))
                AssetDetail.objects.create(asset=asset,customfield=CustomField.objects.get(name='Test_CustomField_{}'.format(i).zfill(4)),value='TestCase Data')

    def test_assetdetail_str(self):
        assetdetail = AssetDetail.objects.first()
        asset = assetdetail.asset
        customfield = assetdetail.customfield
        self.assertEqual(str(assetdetail),'{}-{}'.format(asset,customfield))
        assetdetail = AssetDetail.objects.last()
        self.assertNotEqual(str(assetdetail),'{}-{}'.format(asset,customfield))


    
