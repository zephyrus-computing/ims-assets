from django.conf import settings
from django.urls import re_path
from ims.addons.assets import views

app_name='ims.addons.assets'
urlpatterns = [
    re_path(r'^$', views.asset_list, name='asset_list'),
    re_path(r'^details/$', views.asset_details, name='asset_detail'),
]