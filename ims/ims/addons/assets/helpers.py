from datetime import datetime
from django.conf import settings
from django.contrib.auth.models import User
from django.forms import widgets
from itertools import chain
import logging

from log.logger import getLogger

logger = logging.getLogger(__name__)
logger2 = getLogger('assets')

TEXTINPUT = 'ti'
NUMBERINPUT = 'ni'
EMAILINPUT = 'ei'
URLINPUT = 'ui'
PASSWORDINPUT = 'pi'
HIDDENINPUT = 'hi'
DATEINPUT = 'di'
DATETIMEINPUT = 'ai'
TIMEINPUT = 'ii'
TEXTAREA = 'ta'
CHECKBOXINPUT = 'ci'
SELECT = 'se'
NULLBOOLEANSELECT = 'ns'
SELECTMULTIPLE = 'sm'
RADIOSELECT = 'rs'
CHECKBOXSELECTMULTIPLE = 'cm'
FILEINPUT = 'fi'
CLEARABLEFILEINPUT = 'li'
MULTIPLEHIDDENINPUT = 'mi'
SPLITDATETIMEWIDGET = 'sw'
SPLITHIDDENDATETIMEWIDGET = 'hw'
SELECTDATEWIDGET = 'dw'
WIDGETS = [
    (TEXTINPUT, 'Text Input'),
    (NUMBERINPUT, 'Number Input'),
    (EMAILINPUT, 'Email Input'),
    (URLINPUT, 'URL Input'),
    (PASSWORDINPUT, 'Password Input'),
    (HIDDENINPUT, 'Hidden Input'),
    (DATEINPUT, 'Date Input'),
    (DATETIMEINPUT, 'DateTime Input'),
    (TIMEINPUT, 'Time Input'),
    (TEXTAREA, 'Text Area'),
    (CHECKBOXINPUT, 'Checkbox Input'),
    (SELECT, 'Select'),
    (NULLBOOLEANSELECT, 'Null Boolean Select'),
    (SELECTMULTIPLE, 'Select Multiple'),
    (RADIOSELECT, 'Radio Select'),
    (CHECKBOXSELECTMULTIPLE, 'Checkbox Select Multiple'),
    (FILEINPUT, 'File Input'),
    (CLEARABLEFILEINPUT, 'Clearable File Input'),
    (MULTIPLEHIDDENINPUT, 'Multiple Hidden Input'),
    (SPLITDATETIMEWIDGET, 'Split DateTime Widget'),
    (SPLITHIDDENDATETIMEWIDGET, 'Split Hidden DateTime Widget'),
    (SELECTDATEWIDGET, 'Select Date Widget'),
]

def lookup_dictionary(dic, v):
    for k in dic:
        if k[0] == v:
            return k[1]
    return 'unknown'

def rlookup_dictionary(dic, v):
    for k in dic:
        if k[1] == v:
            return k[0]
    return ''

def get_widget(v):
    return lookup_dictionary(WIDGETS, v)

def get_widget_type(v):
    return rlookup_dictionary(WIDGETS, v)

def get_widget_class(v):
    if len(v) != 2:
        v = rlookup_dictionary(WIDGETS, v)
        if v == '':
            return widgets.TextInput()
    if v == TEXTINPUT: return widgets.TextInput()
    if v == NUMBERINPUT: return widgets.NumberInput()
    if v == EMAILINPUT: return widgets.EmailInput()
    if v == URLINPUT: return widgets.URLInput()
    if v == PASSWORDINPUT: return widgets.PasswordInput()
    if v == HIDDENINPUT: return widgets.HiddenInput()
    if v == DATEINPUT: return widgets.DateInput()
    if v == DATETIMEINPUT: return widgets.DateTimeInput()
    if v == TIMEINPUT: return widgets.TimeInput()
    if v == TEXTAREA: return widgets.Textarea()
    if v == CHECKBOXINPUT: return widgets.CheckboxInput()
    if v == SELECT: return widgets.Select()
    if v == NULLBOOLEANSELECT: return widgets.NullBooleanSelect()
    if v == SELECTMULTIPLE: return widgets.SelectMultiple()
    if v == RADIOSELECT: return widgets.RadioSelect()
    if v == CHECKBOXSELECTMULTIPLE: return widgets.CheckboxSelectMultiple()
    if v == FILEINPUT: return widgets.FileInput()
    if v == CLEARABLEFILEINPUT: return widgets.ClearableFileInput()
    if v == MULTIPLEHIDDENINPUT: return widgets.MultipleHiddenInput()
    if v == SPLITDATETIMEWIDGET: return widgets.SplitDateTimeWidget()
    if v == SPLITHIDDENDATETIMEWIDGET: return widgets.SplitHiddenDateTimeWidget()
    if v == SELECTDATEWIDGET: return widgets.SelectDateWidget()
    return widgets.TextInput()

def parse_querytype(value):
    if value == 1 or value == '1':
        return "^"
    if value == 2 or value == '2':
        return "="
    return ""

def is_int(i):
    try:
        int(i)
        return True
    except ValueError:
        return False
    except TypeError:
        return False