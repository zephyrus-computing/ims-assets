from datetime import datetime
from django.conf import settings
from django.contrib.auth.models import User
from django.db import models
from django.urls import reverse
from itertools import chain
import logging

import ims.addons.assets.helpers as helpers
from inventory.models import Part, Storage, get_sentinel_part, get_sentinel_storage
from log.logger import getLogger

logger = logging.getLogger(__name__)
logger2 = getLogger('assets')

class Asset(models.Model):
    assetid = models.CharField(max_length=256)
    serialnumber = models.EmailField(max_length=256)
    part = models.ForeignKey('inventory.Part',on_delete=models.SET(get_sentinel_part),related_name='+')
    storage = models.ForeignKey('inventory.Storage',on_delete=models.SET(get_sentinel_storage),related_name='+')
    created = models.DateTimeField(auto_now_add=True,editable=False)
    lastmodified = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['assetid','serialnumber']

    def __str__(self):
        return '{}-{}'.format(self.assetid, self.serialnumber)

    def get_absolute_url(self):
        logger.debug('Generating absolute URL for Asset id {}'.format(self.id))
        return reverse('ims.addons.assets:asset_detail', args=[self.id])

    def save(self, *args, **kwargs):
        if self.id != None:
            logger.info('Saving changes made to Asset id {}'.format(self.id))
        else:
            logger.info('Creating new Asset "{}"'.format(self))
        super().save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        logger.info('Deleting Asset id {}'.format(self.id))
        super().delete(*args, **kwargs)

    def move(self, storage):
        try:
            if helpers.is_int(storage):
                storage = Storage.objects.get(id=storage)
            self.storage = storage
            self.save()
            logger.info('Moved Asset id {} to Storage id {}'.format(self.id, storage.id))
        except Storage.DoesNotExist as err:
            logger.warning('Unable to locate Storage \'{}\'. Error mesesage: {}'.format(storage, err))
            raise
        except Exception as err:
            logger.warning('There was an unknown problem in Asset.move() method.')
            logger.debug(err)
            raise

class CustomField(models.Model):
    name = models.CharField(max_length=256)
    description = models.CharField(max_length=256)
    required = models.BooleanField(default=False)
    regex = models.TextField()
    widget = models.CharField(max_length=2,choices=helpers.WIDGETS,default=helpers.TEXTINPUT)

    class Meta:
        ordering = ['name']

    def __str__(self):
        return self.name
    
    def save(self, *args, **kwargs):
        if self.id != None:
            logger.info('Saving changes made to CustomField id {}'.format(self.id))
        else:
            logger.info('Creating new CustomField "{}"'.format(self))
        super().save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        logger.info('Deleting CustomField id {}'.format(self.id))
        super().delete(*args, **kwargs)

class AssetDetail(models.Model):
    asset = models.ForeignKey(Asset,on_delete=models.CASCADE,related_name='details',related_query_name='detail')
    customfield = models.ForeignKey(CustomField,on_delete=models.CASCADE,related_name='+')
    value = models.TextField()

    class Meta:
        ordering = ['asset','customfield']

    def __str__(self):
        return '{}-{}'.format(self.asset.name, self.customfield.name)

    def save(self, *args, **kwargs):
        if self.id != None:
            logger.info('Saving changes made to AssetDetail id {}'.format(self.id))
        else:
            logger.info('Creating new AssetDetail "{}"'.format(self))
        super().save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        logger.info('Deleting AssetDetail id {}'.format(self.id))
        super().delete(*args, **kwargs)