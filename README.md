# IMS-Assets
The Assets add-on module for IMS provides customizable granular details to individual parts. This allows for tracking of an individual part amongst the entirety of your inventory.

## Installation
Quick installation instructions can be found in the Quick Install Wiki section.

Detailed instructions can be found in the Admin Guide Wiki section.

The Debian package can be download from the Package Registry.

## Roadmap
- Initial packaging

## Support
Tickets may be submitted to this repo for support requests or bug reports. You may also email support@zephyruscomputing.com.

## Contributing
Requests to help contribute are welcome. For major changes, please open an issue ticket first to discuss what you would like to change.

Please make sure to update tests as appropriate.

### Development Environment Setup
```
git clone https://gitlab.com/zephyrus-computing/inventory-management-system.git ims
cd ims
./dev.sh setup
cd ..
git clone https://gitlab.com/zephyrus-computing/ims-assets.git ims-assets
cd ims-assets/
./dev-assets.sh setup
cd ..
source venv/bin/activate
```
The dev.sh script with the setup option will configure a python virtual environment and install the packages specified in requirements.txt, create a dev db, and execute the superuser creation process.

The dev-assets.sh script with the setup option will configure the python virtual environment and install the packages specified in the requirements.txt, then add all of the necessary file changes to imitate the installation of IMS-Assets module.

When you have finished your work, you can run ```deactivate``` to close the environment.

If you need to reset your environment, run the following:
```
ims-assets/dev-assets.sh delete
ims/dev.sh delete
ims/dev.sh setup
ims-assets/dev-tracker.sh setup
```

## License
[GPL3](http://choosealicense.com/licenses/gpl-3.0/)
